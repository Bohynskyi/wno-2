import pandas as pd
import numpy as np
import os
from skript.Dataset import Dataset, inputs, output
from skript.Network import NeuralNetwork
import matplotlib.pyplot as plt

PATH_DATA = '{}/data'.format(os.getcwd())

if __name__ == '__main__':
    """
    Program podzieliłem na 4 etapy:
    1) generowanie datasetu - z danych o populacji miast lum państw, 
        oraz statystycznych danych o państwie.
    2) generowanie input oraz output - w tym etapie generuje dane dla neuro sieci.
    3) Uczenie neuro sieci. Uczenie polega na podawaniu danych z poprzedniego dnia i oczekiwanie danych na dzisiejszy dzień.
        !!! UWAGA z braku dobrej wydajności PC nie udało się nauczyć neuro sieć, po 400 krokach nauka dynamika nauki jest dobra (nie dodaje wykresu iż nie udało się zapisać) , tyle kroków zbyt mało żebyś dawać jakieś szczególne wnioski UWAGA !!!
        
    4) sprawdzanie danych, na tym etapie w sumie dajemy na wejście państwo i dane o ludności 
    """

    # Open dataset
    recovered = Dataset(name='recovered', create=False)
    deaths = Dataset(name='deaths', create=False)
    confirmed = Dataset(name='confirmed', create=False)

    # Create dataset
    '''
    recovered = Dataset('{}/Virus_data/time_series_2019-ncov-Recovered.csv'.format(PATH_DATA), name='recovered')
    deaths = Dataset('{}/Virus_data/time_series_2019-ncov-Deaths.csv'.format(PATH_DATA), name='deaths')
    confirmed = Dataset('{}/Virus_data/time_series_2019-ncov-Confirmed.csv'.format(PATH_DATA), name='confirmed')
    '''
    # Create inputs end output data for neuronal network
    inputs = inputs(recovered.drop(), deaths.drop(), confirmed.drop())
    output = output(recovered.drop(), deaths.drop(), confirmed.drop())

    # Neural Network
    neural = NeuralNetwork(inputs, output)

    # train neuronal network
    neural.train()

    # Plot Error
    plt.figure(figsize=(15, 5))
    plt.plot(neural.epoch_list, neural.error_history)
    plt.xlabel('Epoch')
    plt.ylabel('Error')
    plt.show()
    del neural

    """
    4) sprawdzanie danych dla gdańska
    """

    print("END")
