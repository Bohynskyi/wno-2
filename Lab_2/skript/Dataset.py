import pandas as pd
import numpy as np
from tqdm import tqdm


class Dataset:
    def __init__(self, url=None, name="None", create=True):
        self.data = self.open_data(url, name)
        if create:
            self.create(name)

    def create(self, name):
        # Dataset operation
        # Create new columns
        self.data = self.data.assign(Population='None', Life='None', Life_index='None', Safety='None',
                                     Healthcare='None',
                                     Pollution_Index='None', Climate_Index='None')
        self.data.fillna(0, inplace=True)

        #  Population operation
        population_word = pd.read_csv('data/population/WPP2019_TotalPopulationBySex.csv', delimiter=',')
        population_word = population_word[population_word['Time'].isin(['2018'])]
        population_word = population_word[["Location", 'PopTotal']]

        # Create lists
        self.list_country = self.data['Country/Region']
        self.list_state = self.data['Province/State']
        self.list_region = {'US': 'data/population/USA.csv', 'Canada': 'data/population/canada.csv',
                            'United Kingdom': 'data/population/UK.csv',
                            'France': 'data/population/francja.csv', 'China': 'data/population/china.csv',
                            'Australia': 'data/population/australia.csv'}

        # Add the necessary data here
        print("Start : {}".format(name))
        for id_country in tqdm(range(len(self.list_country))):
            country = self.list_country[id_country]  # current state
            try:
                # checks for provinces
                if self.list_state[id_country] == 0:
                    # There are no provinces
                    df = population_word[population_word['Location'].isin(['{}'.format(str(country))])]
                    df = df.to_numpy()

                    self.data.loc[id_country, 'Population'] = int(df[0, 1] * 1000)

                else:
                    # there are provinces
                    self.population_province_state(self.list_region[country], self.list_state[id_country], id_country)
            except:
                self.data.loc[id_country, 'Population'] = None

            self.state_index(country, id_country)

        self.save(name)

    def open_data(self, url, name):
        if url:
            return pd.read_csv(url, delimiter=',')
        else:
            return pd.read_csv("data/dataset/{}.csv".format(name), delimiter=',')

    def save(self, name):
        # Save dataset
        self.data.to_csv("data/dataset/{}.csv".format(name), header=True)

    def state_index(self, country, id_country):
        try:
            if country == "US":
                country = 'United States'
            # State
            index = pd.read_csv('data/index/index.csv', delimiter=',')
            index = index[index['name'].isin(['{}'.format(country)])]

            df = index.to_numpy()
            self.data.loc[id_country, 'Life':'Climate_Index'] = df[0, 1:]

        except:
            self.data.loc[id_country, 'Life':'Climate_Index'] = None

    def population_province_state(self, csv_usl, city, id_country):
        # State
        state = pd.read_csv(csv_usl, delimiter=',')
        state = state[state['admin'].isin(['{}'.format(city)])]
        state = state['population']
        # Sum city
        people = np.sum(state)

        if people == 0:
            self.data.loc[id_country, 'Population'] = None
        else:
            self.data.loc[id_country, 'Population'] = people

    def __repr__(self):
        return self.data

    def drop(self):
        return self.data.dropna()


def inputs(recovered, deaths, confirmed, data_start='1/22/20', data_end="3/22/20"):
    print("Create input end output :")

    result, x, y = reshape(recovered, deaths, confirmed, data_start, data_end)

    result = result.assign(Population='None', Life='None', Life_index='None', Safety='None',
                           Healthcare='None', Pollution_Index='None', Climate_Index='None', Day='None')

    other = recovered.loc[:, 'Population':]
    df = other.to_numpy(dtype='float')
    rez_index = recovered.index

    for i in range(len(rez_index)):
        result.loc[rez_index[i], 'Population':'Climate_Index'] = df[i, :]
        result.loc[rez_index[i], 'Day'] = np.arange(1, (y + 1))

    result = size(result)
    return result


def size(result, input=True):
    result = result.dropna()
    result = result.to_numpy(dtype='float')

    # recovered, deaths, confirmed,
    result[:, :3] = result[:, :3] / 100000
    if input:
        # Population,Life,Life_index,Safety,Healthcare,Pollution_Index,Climate_Index
        result[:, 3:4] = 20000 / result[:, 3:4]
        result[:, 4:] = result[:, 4:]/100000

    return result


def output(recovered, deaths, confirmed, data_start='1/23/20', data_end="3/23/20"):
    result, _, _ = reshape(recovered, deaths, confirmed, data_start, data_end)
    result = size(result, input=False)
    return result


def reshape(recovered, deaths, confirmed, data_start, data_end):
    # ====================================================
    buf_recovered = recovered.loc[:, data_start:data_end]
    buf_deaths = deaths.loc[:, data_start:data_end]
    buf_confirmed = confirmed.loc[:, data_start:data_end]

    x, y = buf_recovered.shape

    # 2d area to 1D
    buf_recovered = buf_recovered.stack()
    buf_deaths = buf_deaths.stack()
    buf_confirmed = buf_confirmed.stack()

    return pd.concat([buf_recovered, buf_deaths, buf_confirmed], axis=1, sort=False), x, y
