import numpy as np  # helps with the math
import random, sys
from tqdm import tqdm
import pandas as pd
from PIL import ImageDraw

PATH = "data/weights"


# create NeuralNetwork skript
class NeuralNetwork:
    # intialize variables in script
    def __init__(self, inputs=0, outputs=0, trening=False):
        # Data
        self.inputs = inputs[:100:, :]
        self.outputs = outputs[:100:, 0:1]

        # Random
        np.random.seed(1)

        # Weights
        self.weights_three = 2 * np.random.rand(11, 10000) - 1
        self.weights_two = 2 * np.random.rand(10000, 10000) - 1
        self.weights_y = 2 * np.random.rand(10000, 1) - 1

        # Error data
        self.error_history = []
        self.epoch_list = []
        self.trening = False

    # activation function ==> S(x) = 1/1+e^(-x)
    def sigmoid(self, x, deriv=False):
        if deriv == True:
            return x * (1 - x)
        return 1 / (1 + np.exp(-x))

    # data will flow through the neural network.
    def feed_forward(self):
        self.hidden_three = self.sigmoid(np.dot(self.inputs, self.weights_three))
        self.hidden_two = self.sigmoid(np.dot(self.hidden_three, self.weights_two))
        self.y = self.sigmoid(np.dot(self.hidden_two, self.weights_y))

    # going backwards through the network to update weights
    def backpropagation(self):
        # Error
        self.error_y = self.outputs - self.y
        self.error_two = np.dot(self.error_y, self.weights_y.T)
        self.error_three = np.dot(self.error_two, self.weights_two.T)

        # 1
        self.delta_three = self.error_three * self.n * self.sigmoid(self.hidden_three, deriv=True)
        adjustments = np.dot(self.inputs.T, self.delta_three)
        self.weights_three += adjustments

        # 2
        self.delta_two = self.error_two * self.n * self.sigmoid(self.hidden_two, deriv=True)
        adjustments = np.dot(self.hidden_three.T, self.delta_two)
        self.weights_two += adjustments

        # 3
        self.delta_three = self.error_y * self.n * self.sigmoid(self.y, deriv=True)
        adjustments = np.dot(self.hidden_two.T, self.delta_three)
        self.weights_y += adjustments

    def save(self):
        # Save
        np.savetxt('data/weights/weights_two.csv', self.weights_two, delimiter=',')
        np.savetxt('data/weights/weights_y.csv', self.weights_y, delimiter=',')
        np.savetxt('data/weights/error_history.csv', self.error_history, delimiter=',')

    # train the neural net for 25,000 iterations
    def train(self, epochs=25000):
        for epoch in tqdm(range(epochs)):
            self.n = 1 / 100000
            # flow forward and produce an output
            self.feed_forward()
            # go back though the network to make corrections based on the output
            self.backpropagation()
            # keep track of the error history over each epoch
            self.error_history.append(np.average(np.abs(self.error_y)))
            self.epoch_list.append(epoch)
        self.save()

    def open(self):
        # Create one natrix weights
        self.weights_two = np.genfromtxt('data/weights/weights_two.csv')
        self.weights_y = np.genfromtxt('data/weights/weights_y.csv')

    # function to predict output on new and unseen input data
    def predict(self, new_input, new_output, test=False):
        # trening neuronal
        inp = new_input / 100000.
        prediction_two = self.sigmoid(np.dot(inp, self.weights_two))
        prediction = self.sigmoid(np.dot(prediction_two, self.weights_y))
        if test:
            request = np.ones([4, 3])
            for i in range(4):
                request[i, 0] = new_output[0, i]
                request[i, 1] = prediction[0, i] * 100
                request[i, 2] = new_output[0, i] - prediction[0, i] * 100
        return request
